#include "hashTable.h"

int hashF(const char name[]) {
	double hashValue = 0;
	for (int i = 0; name[i] != '\0'; i++) {
	   hashValue += (name[i] - 'a') * pow(1.19, i);
	}
	return HTABLE_SIZE*fmod(HTABLE_A*hashValue, 1);
}

void hTableInit(hTable *h)
{
	for (int i = 0; i < HTABLE_SIZE; i++) {
		node *head = (node*)malloc(sizeof(node));
		init(head);
		h->table[i] = head;
	}
}

void hTableInsert(hTable *h, double value, char* name) {
	int i = hashF(name);
	node *pointer = h->table[i]; // head
	if (pointer->next == NULL)
		add_after(pointer, value, name);
	else
	{
		pointer = pointer->next;
		while(pointer != NULL) {
			if (strcmp(pointer->name, name) == 0) {
				pointer->value = value;
				return;
			}
			pointer = pointer->next;
		}
		add_after(h->table[i], value, name);
	}
}

double hTableSearch(hTable *h, char* name) {
	int i = hashF(name);
	node *pointer = h->table[i]->next;
	while (pointer != NULL) {
		if (strcmp(pointer->name, name) == 0) {
			return pointer->value;
		}
		pointer = pointer->next;
	}
	//std::cerr << "ERROR: NIE ZNALAZLEM W HTABLE";
	return -1;
}
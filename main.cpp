#include <stdio.h>
#include <iostream>

#include "lista.h"
#include "hashTable.h"
#include "onp.h"
#include "drzewoObl.h"

#define BUFFER_SIZE 101

void obsluzUstaw(hTable *h)
{
	char name[BUFFER_SIZE];
	std::cin >> name; // nazwa zmiennej
	char x; std::cin >> x; // konsumpcja =
	hTableInsert(h, onpKonwersja(h), name);
}

void obsluzWyznacz(hTable *h)
{
	////wyznacz powinno obliczac prawa strone rownania, po czym wstawic jej wynik
	////za szukana zmienna po lewej stronie, po czym obliczyc lewa strone
	////a wynik wstawic do hashTable pod nazwa szukanej zmiennej

	char name[BUFFER_SIZE];	
	std::cin >> name; // nazwa zmiennej

	onp w;
	w.next = NULL;
	onp *wyjscie = &w;
	zamienNaONP(h, wyjscie, name);
	char x; std::cin >> x; // konsumpcja =

	double prawaStrona = onpKonwersja(h);
	double wynik = tworzDrzewo(h, wyjscie, prawaStrona, name);
	printf("%s = %f\n", name, wynik);
}

void obsluzOblicz(hTable *h)
{
	printf("%.6f\n", onpKonwersja(h));
}

int main(int argc, char * argv[])
{
	char bufor[BUFFER_SIZE] = {};

	hTable h;
	hTable *h_wsk = &h;
	hTableInit(h_wsk);

	while(std::cin >> bufor) 
	{
		if (strcmp(bufor,"USTAW") == 0)
			obsluzUstaw(h_wsk);
		if (strcmp(bufor,"OBLICZ") == 0)
			obsluzOblicz(h_wsk);
		if (strcmp(bufor,"WYZNACZ") == 0)
			obsluzWyznacz(h_wsk);
		if (strcmp(bufor,"KONIEC") == 0)
			break;
	}
	return 0;
}

#include "lista.h"

void init(node *head)
{
	head->next = NULL;
	head->name = NULL;
}

void add_after(node *element, double value, char name[]) //wstawia za element->next
{
	node *ptr = (node*)malloc(sizeof(node));
	ptr->name = (char*)malloc(sizeof(char)*BUFFER_SIZE);
	ptr->value = value;
	strcpy_s(ptr->name, BUFFER_SIZE, name);
	ptr->next = element->next;
	element->next = ptr;
}

void remove_after(node *element) //usuwamy element->next
{
	if(element->next != NULL) //jesli nie jest to ostatni element
	{
		node *ptr = element->next;
		element->next = element->next->next;
		if (ptr->name != NULL) free(ptr->name);	//zwolnienie pamieci na napis
		free(ptr);
	}
}

void length(node *head)
{
	int len = 0;

	while(head->next != NULL)
	{
		len++;
		head = head->next;
	}
	printf("Dlugosc = %d", len);
}

void remove_all(node *head)
{
	if (head->next == NULL)
		return;
	while (head->next != NULL)
	{
		node *wsk = head;
		head = head->next;
		if (wsk->name != NULL) free(wsk->name);
		free(wsk);
	}
	free(head->next);
	head->next = NULL;
}

void push(node *head, double value)
{
	node *ptr = (node*)malloc(sizeof(node));
	ptr->name = NULL;
	ptr->value = value;
	ptr->next = head->next;
	head->next = ptr;
}

void push(node *head, char op)
{
	node *ptr = (node*)malloc(sizeof(node));
	ptr->name = NULL;
	ptr->next = head->next;
	head->next = ptr;
}


node pop(node *head)
{
	node returnNode;
	returnNode.next = NULL;
	returnNode.name = NULL;
	returnNode.value = head->next->value;
	remove_after(head);
	return returnNode;
}

void pushChar(stosCharow *head, char chr) {
	stosCharow *ptr = (stosCharow*)malloc(sizeof(stosCharow));
	ptr->chr = chr;
	ptr->next = head->next;
	head->next = ptr;
}

void pushDouble(stosDoubli *head, double dbl) {
	stosDoubli *ptr = (stosDoubli*)malloc(sizeof(stosDoubli));
	ptr->value = dbl;
	ptr->next = head->next;
	head->next = ptr;
}

char popChar(stosCharow *head) {
	char returnChar = head->next->chr;
	stosCharow *ptr = head->next;
	head->next = head->next->next;
	free(ptr);
	return returnChar;
}

double popDouble(stosDoubli *head) {
	double returnDbl = head->next->value;
	stosDoubli *ptr = head->next;
	head->next = head->next->next;
	free(ptr);
	return returnDbl;
}

void enqueue(onp *head, double value) {
	while(head->next != NULL) {
		head = head->next;
	}
	onp *nowy = (onp*)malloc(sizeof(onp));
	nowy->next = NULL;
	nowy->value = value;
	nowy->op = '\0';
	head->next = nowy;
}
void enqueue(onp *head, char op) {
	while(head->next != NULL) {
		head = head->next;
	}
	onp *nowy = (onp*)malloc(sizeof(onp));
	nowy->next = NULL;
	nowy->op = op;
	head->next = nowy;
}

onp dequeue(onp *head) {
	onp *temp = head->next;
	onp returnStruct;
	returnStruct.op = temp->op; returnStruct.value = temp->value; returnStruct.next = NULL;
	head->next = head->next->next;
	free(temp);
	return returnStruct;
}

void pushONP(onp *head, double value) {
	onp *ptr = (onp*)malloc(sizeof(onp));
	ptr->value = value;
	ptr->op = '0';
	ptr->next = head->next;
	head->next = ptr;
}
void pushONP(onp *head, char op) {
	onp *ptr = (onp*)malloc(sizeof(onp));
	ptr->op = op;
	ptr->next = head->next;
	head->next = ptr;
}




#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <cstdlib>
#include <cstring>
#include <cmath>

#include "lista.h"

#define HTABLE_SIZE 92623
#define HTABLE_A 0.61803398874989484820458683436564

typedef struct hTable {
	node *table[HTABLE_SIZE];
} hTable;

void hTableInit(hTable *h);
void hTableInsert(hTable *h, double value, char* name);

int hashF(const	char name[]);

//zakladamy ze zawsze znajdziemy wartosc
double hTableSearch(hTable *h, char* name);

#endif
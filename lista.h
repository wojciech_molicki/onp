#ifndef LISTA_H
#define LISTA_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define BUFFER_SIZE 101

typedef struct node {
	double value;
	char *name;
	node *next;
} node;

void init(node *head);
void add_after(node *element, double value, char name[]);
void remove_after(node *element);
void length(node *head);
void remove_all(node *head);

//stos
void push(node *element, double value);
node pop(node *head);


typedef struct stosDoubli {
	double value;
	stosDoubli *next;
} stosDoubli;

typedef struct stosCharow {
	char chr;
	stosCharow *next;
} stosCharow;

void pushChar(stosCharow *head, char chr);
void pushDouble(stosDoubli *head, double dbl);
char popChar(stosCharow *head);
double popDouble(stosDoubli *head);


typedef struct onp {
	double value;
	char op;
	onp *next;
} onp;

void enqueue(onp *head, double value);
void enqueue(onp *head, char op);
void pushONP(onp *head, double value);
void pushONP(onp *head, char op);

onp dequeue(onp *head);

#endif
#ifndef ONP_H
#define ONP_H

#include <cstdio>
#include <iostream>

#include "lista.h"
#include "hashTable.h"

#define BUFFER_SIZE 101

double onpKonwersja(hTable *h);

void obliczWNawiasie(stosDoubli *stos_liczb_ptr, stosCharow *stos_operatorow_ptr);

void zamienNaONP(hTable *h, onp *wynik, char *szukana);

int getPriority(char op);

#endif
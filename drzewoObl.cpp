#include "drzewoObl.h"

void wezelInit(wezel* w) {
	w->left = NULL;
	w->right = NULL;
	w->op = '0';
}

double tworzDrzewo(hTable *h, onp *wejscie, double prawaStrona, char* szukana)
{
	wezel *root = NULL;

	onp s_w;
	onp *stos_wezlow = &s_w;

	stos_wezlow->next = NULL;
	onp *head_wejscie = wejscie;

	wejscie = wejscie->next;
	while(wejscie != NULL)
	{
		wejscie = wejscie->next;
		// tu moze sie pojawic x
		onp onp_op = dequeue(head_wejscie);

		if (onp_op.op == 'x')
		{
			pushONP(stos_wezlow, 'x');
			continue;
		}

		if (onp_op.op != '\0') {
			// tu tez moze sie pojawic x
			onp do_wezla1 = dequeue(stos_wezlow);
			onp do_wezla2 = dequeue(stos_wezlow);

			// pushuje wartosc jesli moze byc obliczona
			if (do_wezla1.op != 'x' && do_wezla2.op != 'x') {
				pushONP(stos_wezlow, wynik(do_wezla2.value, do_wezla1.value, onp_op.op));
			}
			else if (root == NULL) {
				// jeden z do_wezla zawiera niewiadoma, musimy dowiazac do nowego wezla
				wezel *left; wezel *right; wezel *dzialanie;
				left = (wezel*)malloc(sizeof(wezel));
				right = (wezel*)malloc(sizeof(wezel));
				dzialanie = (wezel*)malloc(sizeof(wezel));

				wezelInit(left); wezelInit(right); wezelInit(dzialanie);

				dzialanie->op = onp_op.op;
				dzialanie->left = left; dzialanie->right = right;

				if (do_wezla1.op == 'x') {
					left->op = 'x';
					right->value = do_wezla2.value; 
				}
				else { 
					right->op = 'x';
					left->value = do_wezla1.value;
				}

				root = dzialanie;

				pushONP(stos_wezlow, 'x');
			}
			// gdy root juz istnieje, musimy dowiazac nowy zestaw do niego
			else {
				wezel *dzialanie; wezel *lisc;
				dzialanie = (wezel*)malloc(sizeof(wezel));
				lisc = (wezel*)malloc(sizeof(wezel));
				wezelInit(dzialanie); wezelInit(lisc);

				dzialanie->op = onp_op.op;

				//sprawdzenie ktory z operandow to niewiadoma, i odpowiednie dowiazanie
				if (do_wezla1.op == 'x') {
					dzialanie->left = root;
					lisc->value = do_wezla2.value;
					dzialanie->right = lisc;
				}
				else {
					dzialanie->right = root;
					lisc->value = do_wezla1.value;
					dzialanie->left = lisc;
				}

				root = dzialanie;
				pushONP(stos_wezlow, 'x');
			}
		}
		else {
			// jesli nie operator, to pushuj na stos wezlow wartosc
			if (onp_op.op != 'x')
				pushONP(stos_wezlow, onp_op.value);
			else {
				pushONP(stos_wezlow, 'x');
			}
		}
	}
	double wynik = wyznaczX(root, prawaStrona);
	hTableInsert(h, wynik, szukana);
	return wynik;
}

double wyznaczX(wezel *root, double prawaStrona)
{
	while (root != NULL && root->left != NULL && root->right != NULL)
	{
		if (root->left->op == '0') {
			prawaStrona = wyliczPrawostronnie(root->left->value, prawaStrona, root->op);
		}
		if (root->right->op == '0') {
			prawaStrona = wyliczLewostronnie(root->right->value, prawaStrona, root->op);
		}

		wezel *wsk = root;
		if (root->left->op != '0')	{
			free(root->right);
			root->right = NULL;
			root = root->left;
		}
		else {
			free(root->left);
			root->left = NULL;
			root = root->right;
		}
		free(wsk);
	}

	return prawaStrona;
}

double wynik(double val1, double val2, char op)	{
	switch (op) {
	case '+':
		return val1 + val2;
		break;
	case '*':
		return val1 * val2;
		break;
	case '-':
		return val1 - val2;
		break;
	case '/':
		return val1 / val2;
		break;
	default:
		return -1;
		break;
	};
}

double wyliczPrawostronnie(double val1, double prawaStrona, char op) {
	switch (op) {
	case '+':
		return prawaStrona - val1;
		break;
	case '*':
		return prawaStrona / val1;
		break;
	case '-':
		return val1 + prawaStrona;
		break;
	case '/':
		return val1 * prawaStrona;
		break;
	default:
		return -1;
		break;
	};
}

double wyliczLewostronnie(double val1, double prawaStrona, char op) {
	switch (op) {
	case '+':
		return prawaStrona - val1;
		break;
	case '*':
		return prawaStrona / val1;
		break;
	case '-':
		return val1 - prawaStrona;
		break;
	case '/':
		return val1 / prawaStrona;
		break;
	default:
		return -1;
		break;
	};
}
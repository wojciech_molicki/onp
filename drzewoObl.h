#ifndef DRZEWOOBL_H
#define DRZEWOOBL_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "lista.h"
#include "hashTable.h"

typedef struct wezel {
	wezel *left;
	wezel *right;
	char op;
	double value;
} wezel;

void wezelInit(wezel* w);

double tworzDrzewo(hTable *h, onp *wejscie, double prawaStrona, char* szukana);
double wyznaczX(wezel *root, double prawaStrona);

double wynik(double val1, double val2, char op);
double wyliczPrawostronnie(double val1, double prawaStrona, char op);
double wyliczLewostronnie(double val1, double prawaStrona, char op);

#endif
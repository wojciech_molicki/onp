#include "onp.h"

int getPriority(char op)
{
	if (op == '+' || op == '-') return 1;
	else if (op == '*' || op == '/') return 2;
	return -1;
}

double onpKonwersja(hTable *h) {
	char bufor[BUFFER_SIZE];
	double value;

	stosDoubli stos_liczb;
	stosDoubli *stos_liczb_ptr = &stos_liczb;
	stos_liczb_ptr->next = NULL;

	stosCharow stos_operatorow;
	stosCharow *stos_operatorow_ptr = &stos_operatorow;
	stos_operatorow_ptr->next = NULL;

	std::cin >> bufor; // skonsumuj pierwszy nawias
	pushChar(stos_operatorow_ptr, bufor[0]);

	int nawiasCount = 1;
	while(nawiasCount > 0)
	{
		std::cin >> bufor;

		if (bufor[0] >= 'a' && bufor[0] <= 'z')
		{
			value = hTableSearch(h, bufor);
			pushDouble(stos_liczb_ptr, value);
			continue;
		}
		//jesli operand przepisz na wyjscie
		else if ((bufor[0] <= '9' && bufor[0] >= '0') ||
			(bufor[0] == '-' && bufor[1] <= '9' && bufor[1] >= '0'))
		{
			value = atof(bufor);
			pushDouble(stos_liczb_ptr, value);
			continue;
		}
		else if (bufor[0] == ')') {
			nawiasCount--;
			obliczWNawiasie(stos_liczb_ptr, stos_operatorow_ptr);
			if (nawiasCount == 0) break;
		}

		else if (bufor[0] == '(')
		{
			nawiasCount++;
			pushChar(stos_operatorow_ptr, bufor[0]);
		}
		// jesli operator
		else
		{
			int priority = getPriority(bufor[0]);
			char top = stos_operatorow_ptr->next->chr;
			if (getPriority(top) >= priority)
			{
				obliczWNawiasie(stos_liczb_ptr, stos_operatorow_ptr);
			}
			pushChar(stos_operatorow_ptr, bufor[0]);
		}
	}
	while(stos_operatorow_ptr->next != NULL)
	{
		obliczWNawiasie(stos_liczb_ptr, stos_operatorow_ptr);
	}
	return popDouble(stos_liczb_ptr);
}

void obliczWNawiasie(stosDoubli *stos_liczb_ptr, stosCharow *stos_operatorow_ptr) {
	while(stos_operatorow_ptr->next != NULL)
	{
		char op = popChar(stos_operatorow_ptr);
		// tu musisz wykonywa� obliczenia
		if (op == '(') { return; }
		else
		{
			double op2 = popDouble(stos_liczb_ptr);
			double op1 = popDouble(stos_liczb_ptr);

			switch(op) {
			case '+':
				pushDouble(stos_liczb_ptr, op1+op2);
				break;
			case '-':
				pushDouble(stos_liczb_ptr, op1-op2);
				break;
			case '*':
				pushDouble(stos_liczb_ptr, op1*op2);
				break;
			case '/':
				pushDouble(stos_liczb_ptr, op1/op2);
				break;
			}
		}
	}
}

void zamienNaONP(hTable *h, onp *wynik, char *szukana) {
	char bufor[BUFFER_SIZE];
	std::cin >> bufor; //konsumpcja pierwszego nawiasu

	double value;
	onp s;
	onp *stos = &s;
	stos->next = NULL;
	onp *tail = wynik;

	enqueue(stos, bufor[0]);

	int nawiasCount = 1;
	while(nawiasCount > 0)
	{
		std::cin >> bufor;

		if (bufor[0] >= 'a' && bufor[0] <= 'z')
		{
			if (strcmp(bufor, szukana) == 0) {
				enqueue(tail, 'x');
				tail = tail->next;
				continue;
			}
			value = hTableSearch(h, bufor);
			enqueue(tail, value);
			tail = tail->next;
			continue;
		}
		//jesli operand przepisz na wyjscie
		else if ((bufor[0] <= '9' && bufor[0] >= '0') ||
			(bufor[0] == '-' && bufor[1] <= '9' && bufor[1] >= '0'))
		{
			value = atof(bufor);
			enqueue(tail, value);
			tail = tail->next;
			continue;
		}
		else if (bufor[0] == ')') {
			nawiasCount--;
			while(stos->next != NULL)
			{
				onp temp = dequeue(stos);
				if (temp.op == '(') break;
				enqueue(tail, temp.op);
				tail = tail->next;
			}
			if (nawiasCount == 0) break;
		}
		else if (bufor[0] == '(')
		{
			nawiasCount++;
			pushONP(stos, bufor[0]);
		}
		else
		{
			int priority = getPriority(bufor[0]);
			while(stos->next != NULL)
			{
				char top = stos->next->op;
				if (top == '(') break;
				if (getPriority(top) < priority) break;
				enqueue(tail, dequeue(stos).op);
				tail = tail->next;
			}
			pushONP(stos, bufor[0]);
		}
	}
	while(stos->next != NULL)
	{
		pushONP(wynik, dequeue(stos).op);
	}
}